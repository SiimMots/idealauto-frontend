import fetch from 'node-fetch';

export default async function sendMail(data) {
  return await fetch(`http://127.0.0.1:8000/api/contact-us`, {
    method: 'POST',
    mode: 'cors',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })
    .then((res) => res.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log('ERROR', error);
    });
}
