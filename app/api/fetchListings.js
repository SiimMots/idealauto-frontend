import fetch from 'node-fetch';

export default async function fetchListings() {
  const listings = await fetch(`http://127.0.0.1:8000/api/listings`, {
    method: 'GET',
    mode: 'cors',
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  }).then((res) => {
    if (!res.ok) throw res;
    if (res.headers.get('content-type').includes('application/json')) return res.json();
    return res.text();
  });

  if (listings.exception) throw listings;
  if (listings.to_url && listings.from_url) return { redirect: listings };

  return {
    listings,
  };
}
