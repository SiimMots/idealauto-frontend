import React from 'react';

import bigCar from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF0279.jpg';
import smallCar1 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF7228-1.jpg';
import smallCar2 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF8954.jpg';
import smallCar3 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF9482-1.jpg';
import smallCar4 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF9625_resize.JPG';

export default function SectionContent() {
  return (
    <div className="cars_container">
      <div className="single-container">
        <div className="big-car">
          <div style={{ backgroundImage: `url(${bigCar})` }}></div>
        </div>
      </div>

      <div className="multi-container">
        <div className="small-car">
          <div style={{ backgroundImage: `url(${smallCar1})` }}></div>
        </div>
        <div className="small-car">
          <div style={{ backgroundImage: `url(${smallCar2})` }}></div>
        </div>
        <div className="small-car">
          <div style={{ backgroundImage: `url(${smallCar3})` }}></div>
        </div>
        <div className="small-car">
          <div style={{ backgroundImage: `url(${smallCar4})` }}></div>
        </div>
      </div>
    </div>
  );
}
