import { useState, useRef, useEffect, React } from 'react';

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false);

  function toggle() {
    setIsShowing(!isShowing);
  }

  return {
    isShowing,
    toggle,
  };
};

export default useModal;

/*
export default function ListingsSeciton({ listings }) {
  const [state, setState] = useState([]);
  const [sortType, setSortType] = useState('');
  const [isDescending, setOrderDirection] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 300);
  const ref = useRef();
  const { isShowing, toggle } = useModal();

  const escape = useKey('escape');

  useOnClickOutside(ref, () => toggle);
  console.log(isShowing);

  let sortedListings = listings.listings
    .sort((a, b) => {
      if (isDescending === true) {
        sortedListings = b[sortType] - a[sortType];
      } else {
        sortedListings = a[sortType] - b[sortType];
      }
      return sortedListings;
    })
    .filter((listing) => {
      return listing.model.toLowerCase().indexOf(debouncedSearchTerm.toLowerCase()) !== -1;
    });

  const handleSort = (event) => {
    const type = event.target.value;

    if (sortType === type) {
      setOrderDirection((previousDirection) => !previousDirection);
      setState(!state);
    } else {
      setSortType(type);
    }
  };

  const props = useSpring({
    to: async (next) => {
      await next({ opacity: 0, marginTop: -200 });
      await next({ opacity: 1, marginTop: 0 });
    },
  });

  return (
    <div className="listings">
      <div className="listings-container">
        <div className="listings-header">
          <div className="vehicles-wrapper">
            <input className="search" placeholder="Otsing" onChange={(e) => setSearchTerm(e.target.value)} />

            <div className="big-column"> Mark ja mudel </div>
            <button value="mileage" onClick={handleSort} className="column">
              {sortType === 'mileage' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'mileage' && isDescending === false ? <svg className="sort-asc" /> : ' '} Läbisõit{' '}
            </button>
            <div className="column"> Keretüüp </div>
            <div className="column"> Vedav sild </div>
            <button value="year" onClick={handleSort} className="column">
              {sortType === 'year' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'year' && isDescending === false ? <svg className="sort-asc" /> : ' '} Aasta{' '}
            </button>
            <div className="column"> Kütus </div>
            <div className="column"> Käigukast</div>
            <button value="price" onClick={handleSort} className="column">
              {sortType === 'price' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'price' && isDescending === false ? <svg className="sort-asc" /> : ' '} Hind{' '}
            </button>
          </div>

          {sortedListings.map((
            item,
            id // looping through and displaying all the listings
          ) => (
            <div className="listings-wrapper" key={id}>
              <animated.div className="listing" style={props}>
                <div className="big-information">
                  {' '}
                  <div className="car">
                    <div>
                      {isShowing ? (
                        <div ref={ref}>
                          {' '}
                          <div className="modal-container">
                            <Modal isShowing={isShowing} hide={toggle} item={item} />
                          </div>
                          {escape && toggle()}
                        </div>
                      ) : (
                        <button className="button-default" onClick={toggle}>
                          {' '}
                          <img src={item.thumbnail} style={{ width: '80px', height: '60px' }} />{' '}
                        </button>
                      )}
                    </div>
                  </div>
                </div>
                <div className="name">{item.model}</div>

                <div className="information"> {item.bodyType}</div>
                <div className="information"> {item.drive}</div>
                <div className="information"> {item.mileage + ' km'} </div>
                <div className="information"> {item.year}</div>
                <div className="information"> {JSON.parse(item.fuelType)}</div>
                <div className="information"> {JSON.parse(item.gearbox)}</div>
                <div className="information"> {item.price + ' eur'}</div>
              </animated.div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

function useDebounce(value, delay) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

// Click hook

function useOnClickOutside(ref, handler) {
  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);

      console.log(event);
    };
    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}

// Key hook
function useKey(key) {
  const [pressed, setPressed] = useState(false);

  const match = (event) => key.toLowerCase() === event.key.toLowerCase();

  const onDown = (event) => {
    if (match(event)) setPressed(true);
  };

  const onUp = (event) => {
    if (match(event)) setPressed(false);
  };

  useEffect(() => {
    window.addEventListener('keydown', onDown);
    window.addEventListener('keyup', onUp);
    return () => {
      window.removeEventListener('keydown', onDown);
      window.removeEventListener('keyup', onUp);
    };
  }, [key]);
  return pressed;
}


*/
