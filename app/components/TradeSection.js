import React from 'react';

export default function SectionContent({ tradeContent }) {
  // const trade = content.data.content.find((item) => item.layout === "autovahetus");

  const bulletpoints = tradeContent.bulletpoints;

  const title = tradeContent.pealkiri;

  const firstWord = title.split(' ').slice(0, 1).join(' ');

  const otherWords = title.split(' ').slice(1, 3).join(' ');

  return (
    <div className="vahetus">
      <div className="vahetus-honeycomb">
        <div className="honeycomb2"> </div>
      </div>

      <div className="content_right">
        <div className="vahetus_container">
          <div className="vahetus-heading">
            <p>
              <span> {firstWord} </span>
              {otherWords}
            </p>
          </div>

          <div className="vahetus_rectangle"></div>
        </div>

        <div className="vahetus_container">
          <div className="vahetus-text">
            <p>{tradeContent.kirjeldus}</p>
          </div>
        </div>

        <div className="vahetus_container">
          <div className="vahetus-info">
            <div className="info-flex">
              <div className="list_item">
                {bulletpoints.map((
                  item,
                  key // loop teenused
                ) => (
                  <div key={key}>
                    <div className="diamond"></div>
                    <div className="diamond_container">
                      {' '}
                      <p>{item.attributes.bulletpoint}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="vahetus_container">
          <div className="vahetus-buttons">
            <ul className="buttons">
              <li className="button1">
                <p> {tradeContent.nupud[0].attributes.nupu_pealkiri}</p>
              </li>
              <li className="button2">
                <p>{tradeContent.nupud[1].attributes.nupu_pealkiri}</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
