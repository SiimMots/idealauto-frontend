import React from 'react';

export default function SectionContent({ contactContent }) {
  const info = contactContent.andmed;

  return (
    <div className="page">
      <div className="overlay-image-wrapper">
        <div className="overlay-image"></div>
      </div>
      <div className="contactWrapper">
        <div className="contactContainer">
          <div className="heading">
            {' '}
            <h1> {contactContent.title}</h1>
          </div>
          <div className="rectangle"></div>
          <div className="contact-table">
            <div className="service">
              <div className="icon-container">
                <div className="service-icon"></div>
              </div>
              <div className="title"> {info[0].attributes.pealkiri}</div>
              <div className="info"> {info[0].attributes.kirjeldus}</div>
            </div>
            <div className="sale">
              <div className="icon-container">
                <div className="sale-icon"></div>
              </div>
              <div className="title"> {info[1].attributes.pealkiri}</div>
              <div className="info"> {info[1].attributes.kirjeldus}</div>
            </div>
            <div className="carwash">
              <div className="icon-container">
                <div className="carwash-icon"></div>
              </div>
              <div className="title"> {info[2].attributes.pealkiri}</div>
              <div className="info"> {info[2].attributes.kirjeldus}</div>
            </div>
            <div className="address">
              <div className="icon-container">
                <div className="address-icon"></div>
              </div>
              <div className="title"> {info[3].attributes.pealkiri}</div>
              <div className="info"> {info[3].attributes.kirjeldus}</div>
            </div>

            <div className="phone">
              <div className="icon-container">
                <div className="phone-icon"></div>
              </div>
              <div className="title"> {info[4].attributes.pealkiri}</div>
              <a className="info" href={info[4].attributes.kirjeldus}>
                {info[4].attributes.kirjeldus}
              </a>
            </div>
            <div className="email">
              <div className="icon-container">
                <div className="email-icon"></div>
              </div>
              <div className="title"> {info[5].attributes.pealkiri}</div>
              <div className="info"> {info[5].attributes.kirjeldus}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
