import React from 'react';
// import { AppDataContext } from "@optimistdigital/create-frontend/universal-react";

export default function SectionContent({ aboutContent }) {
  // const aboutSection = content.data.content.find((item) => item.layout === "firmast");

  return (
    <div className="firmast">
      <div className="firmast_table">
        <div className="firmast_table_header">
          <p> {aboutContent.title}</p>
          <div className="long_rectangle"></div>
        </div>
        <div className="squares">
          <div className="square"></div>
          <div className="square"></div>
          <div className="square"></div>
        </div>
        <div className="firmast_text">
          <p>{aboutContent.subtitle}</p>
        </div>

        <div className="firmast_button">
          <p> {aboutContent.nupud[0].attributes.nupu_pealkiri} </p>
        </div>
      </div>
    </div>
  );
}
