import React, { useState, useEffect } from 'react';
import fetchListings from '../api/fetchListings';
import ListingsSection from './ListingsSection';
import Errorpage from '../pages/ErrorPage';

function UsedCarListings() {
  const [data, setData] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchListings();
      setData(result);
    };
    fetchData();
  }, []);
  if (data === null) return <Errorpage />;
  return <ListingsSection listings={data} />;
}
export default UsedCarListings;
