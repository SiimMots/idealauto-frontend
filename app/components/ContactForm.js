import React, { useState } from 'react';

import sendMail from '../api/sendMail';
import TextareaAutosize from 'react-textarea-autosize';

export default function ContactForm({ formInfo = '', formContent }) {
  const [errorsMessages, setErrorMessages] = useState(null);
  const [mailSent, setMailSent] = useState(false);

  const form = formContent.emailivorm;

  const confirmationMessage = formContent.sõnum;

  async function sendEmail(event) {
    event.preventDefault();
    event.persist();
    const data = {
      name: event?.target?.name?.value ?? null,
      email: event?.target?.email?.value ?? null,
      phone: event?.target?.topic?.value ?? null,
      content: event?.target?.content?.value ?? null,
      extra: formInfo,
    };

    const response = await sendMail(data);

    if (response.errors) {
      setMailSent(false);
      setErrorMessages(Object.values(response.errors));
      return false;
    }

    setErrorMessages(null);
    setMailSent(true);
    event.target.reset();
    return false;
  }

  return (
    <div className="email-container">
      <div className="inner-email-container">
        <div className="maxwidth-wrapper">
          <div className="email-wrapper">
            <div className="email-content">
              <div className="email-header"> {formContent.title}</div>
              <form id="email-form" onSubmit={sendEmail}>
                <div className="inputs">
                  <div className="info-container">
                    <div className="info-input">
                      <div className="input-header"> {form[0].attributes.title}</div>
                      <FormInputField type={'text'} name={'name'} title={'Nimi'}></FormInputField>
                    </div>
                    <div className="info-input">
                      <div className="input-header">{form[1].attributes.title}</div>
                      <FormInputField type={'text'} name={'phone'} title={'Telefon'}></FormInputField>
                    </div>
                    <div className="info-input">
                      <div className="input-header">{form[2].attributes.title}</div>
                      <FormInputField type={'email'} name={'email'} title={'Email'}></FormInputField>
                    </div>
                  </div>
                  <div className="message-container">
                    <div className="message-input">
                      <div className="input-header">{form[3].attributes.title}</div>
                      <FormTextAreaField type={'text'} name={'content'} title={'Sõnum'} />
                    </div>
                  </div>
                </div>
                <div className="send-container">
                  <div className="button-container">
                    <div className="button">
                      <input type="submit" value="saada" />
                    </div>
                  </div>
                </div>
              </form>

              <div className="sent-message"> {mailSent ? confirmationMessage : ''}</div>
              {errorsMessages && (
                <div className={'form-errors'}>
                  {Object.values(errorsMessages).map((error, index) => {
                    return <p key={index}>{error}</p>;
                  })}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  function FormInputField({ type, name, label }) {
    return (
      <li className={'form-field'}>
        <label className={'form-label'}> {label}</label>
        <input type={type} name={name} />
        <br />
      </li>
    );
  }

  function FormTextAreaField({ label, name }) {
    return (
      <li className={' form-field'}>
        <label className={'form-field'}> {label}</label>
        <TextareaAutosize name={name}></TextareaAutosize>
      </li>
    );
  }
}
