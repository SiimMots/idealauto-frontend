import React, { useState, useEffect, useRef } from 'react';

import { useSpring, animated } from 'react-spring';
import _ from 'lodash';
import Modal from './Modal';
import useModal from './useModal';

export default function ListingsSeciton({ listings }) {
  const [state, setState] = useState([]);
  const [sortType, setSortType] = useState('');
  const [isDescending, setOrderDirection] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 300);
  const ref = useRef();
  const { isShowing, toggle } = useModal();
  const [listingId, setListingId] = useState('');

  const escape = useKey('escape');
  const left = useKey('ArrowLeft');
  const right = useKey('ArrowRight');

  // Can change listing modals with arrowkeys

  // Key hook
  function useKey(key) {
    const [pressed, setPressed] = useState(false);

    const match = (event) => key.toLowerCase() === event.key.toLowerCase();

    const onDown = (event) => {
      if (match(event)) setPressed(true);
    };

    const onUp = (event) => {
      if (match(event)) setPressed(false);
    };

    useEffect(() => {
      window.addEventListener('keydown', onDown);
      window.addEventListener('keyup', onUp);
      return () => {
        window.removeEventListener('keydown', onDown);
        window.removeEventListener('keyup', onUp);
      };
    }, [key]);
    return pressed;
  }

  useEffect(() => {
    if (isShowing && listingId > 1 && left) {
      setListingId(listingId - 1);
    }
  }, [left]);
  useEffect(() => {
    if (isShowing && listingId < sortedListings.length && right) {
      setListingId(listingId + 1);
    }
  }, [right]);

  // Closes the modal when clicking out of it.

  useOnClickOutside(ref, () => toggle);

  // Sorts listings

  let sortedListings = listings.listings
    .sort((a, b) => {
      if (isDescending === true) {
        sortedListings = b[sortType] - a[sortType];
      } else {
        sortedListings = a[sortType] - b[sortType];
      }
      return sortedListings;
    })
    .filter((listing) => {
      return listing.model.toLowerCase().indexOf(debouncedSearchTerm.toLowerCase()) !== -1;
    });

  const handleSort = (event) => {
    const type = event.target.value;

    if (sortType === type) {
      setOrderDirection((previousDirection) => !previousDirection);
      setState(!state);
    } else {
      setSortType(type);
    }
  };

  // Listings animations

  const props = useSpring({
    to: async (next) => {
      if (!isShowing) {
        await next({ opacity: 0, marginTop: -150, color: '#FFFFF' });
        await next({ opacity: 1, marginTop: 0, color: '#000000' });
      }
    },
  });

  // Handles click on listings

  function handleClick(item) {
    setListingId(item.id);

    return toggle();
  }

  return (
    <div className="listings">
      <div className="listings-container">
        <Modal
          isShowing={isShowing}
          hide={toggle}
          listingId={listingId}
          data={sortedListings} /* Renders the modal */
        />
        <div className="listings-header">
          <div className="vehicles-wrapper">
            <input className="search" placeholder="Otsing" onChange={(e) => setSearchTerm(e.target.value)} />

            <div className="big-column"> Mark ja mudel </div>
            <button value="mileage" onClick={handleSort} className="column">
              {sortType === 'mileage' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'mileage' && isDescending === false ? <svg className="sort-asc" /> : ' '} Läbisõit{' '}
            </button>
            <div className="column"> Keretüüp </div>
            <div className="column"> Vedav sild </div>
            <button value="year" onClick={handleSort} className="column">
              {sortType === 'year' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'year' && isDescending === false ? <svg className="sort-asc" /> : ' '} Aasta{' '}
            </button>
            <div className="column"> Kütus </div>
            <div className="column"> Käigukast</div>
            <button value="price" onClick={handleSort} className="column">
              {sortType === 'price' && isDescending === true ? <svg className="sort-desc" /> : ' '}
              {sortType === 'price' && isDescending === false ? <svg className="sort-asc" /> : ' '} Hind{' '}
            </button>
          </div>

          {sortedListings.map((item, id) => (
            <>
              {isShowing ? (
                <div ref={ref}>
                  {' '}
                  <div className="modal-container"></div>
                  {escape && toggle()}
                </div>
              ) : (
                <div></div>
              )}
              <div className="listings-wrapper">
                <animated.div className="listing" style={props} onClick={() => handleClick(item)} key={id}>
                  <div className="big-information">
                    {' '}
                    <div className="car">
                      <div>
                        <button className="button-default">
                          {' '}
                          <img src={item.thumbnail} style={{ width: '80px', height: '60px' }} />{' '}
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="name">{item.model}</div>
                  <div className="information"> {item.mileage + ' km'} </div>
                  <div className="information"> {item.bodyType}</div>
                  <div className="information"> {item.drive}</div>
                  <div className="information"> {item.year}</div>
                  <div className="information"> {JSON.parse(item.fuelType)}</div>
                  <div className="information"> {JSON.parse(item.gearbox)}</div>
                  <div className="information"> {item.price + ' eur'}</div>
                </animated.div>
              </div>
              <div className="separator" />
            </>
          ))}
        </div>
      </div>
    </div>
  );
}

// Debounce hook

function useDebounce(value, delay) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}

// Click hook

function useOnClickOutside(ref, handler) {
  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}
