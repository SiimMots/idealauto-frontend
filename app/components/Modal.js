import React, { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Link, useParams } from 'react-router-dom';

const Modal = ({ isShowing, hide, listingId, data }) => {
  const ref = useRef();

  const index = Number(listingId) - 1;

  useOnClickOutside(ref, () => hide());

  return isShowing
    ? ReactDOM.createPortal(
        <React.Fragment>
          <div className="modal-overlay" />
          <div className="modal-wrapper" aria-modal aria-hidden tabIndex={-1} role="dialog">
            <div ref={ref} className="modal">
              <div className="modal-header">
                <div className="thumbnail">
                  <img src={data[index].thumbnail} style={{ width: '300xp', height: '240px' }} />
                </div>
                <div className="link">
                  <a href={'/listing/' + listingId}>Täisvaade</a>
                </div>
                <button type="button" className="modal-close-button" onClick={hide}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-content">
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Mark ja mudel</span>
                  <text>{data[index].model}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Hind</span>
                  <text>{data[index].mileage + ' KM'}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Keretüüp</span>
                  <text>{data[index].bodyType}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Vedav sild</span>
                  <text>{data[index].drive}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Aasta</span>
                  <text>{data[index].year}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Kütus</span>
                  <text>{JSON.parse(data[index].fuelType)}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Käigukast</span>
                  <text>{JSON.parse(data[index].gearbox)}</text>
                </div>
                <div className="modal-seperator"></div>
                <div className="modal-info">
                  <span> Hind</span>
                  <text>{data[index].price + ' EUR'}</text>
                </div>
                <div className="modal-seperator"></div>
              </div>
            </div>
          </div>
        </React.Fragment>,
        document.body
      )
    : null;
};

function useOnClickOutside(ref, handler) {
  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}

export default Modal;
