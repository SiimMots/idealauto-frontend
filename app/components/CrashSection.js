import React from 'react';
// import { AppDataContext } from "@optimistdigital/create-frontend/universal-react";

export default function SectionContent({ crashContent }) {
  // const crashContent = content.data.content.find((item) => item.layout === "avarii");

  const bulletpoints = crashContent.bulletpoints;

  return (
    <div className="avarii">
      <div className="avarii_image_container">
        <div className="avarii_image_honeycomb"> </div>
      </div>

      <div className="avarii_container">
        <div className="avarii_table">
          <div className="avarii_table_header">
            <p>{crashContent.pealkiri}</p>
          </div>

          <div className="avarii_table_rectangle"></div>

          <div className="avarii_table_list">
            {bulletpoints.map((
              item,
              key // loop teenused
            ) => (
              <div className="avarii_list_item" key={key}>
                <div className="checkmark"></div>
                {item.attributes.bulletpoint}
              </div>
            ))}
          </div>
          <div className="avarii_table_buttons">
            <ul className="avarii_buttons">
              <li className="button1">
                {' '}
                <p> {crashContent.nupud[0].attributes.nupu_pealkiri}</p>
              </li>
              <li className="button3">
                {' '}
                <p> {crashContent.nupud[1].attributes.nupu_pealkiri}</p>
              </li>
            </ul>
          </div>

          <div className="kahjuavaldus_container">
            <div className="chevron"></div>
            <div className="kahjuavaldus">{crashContent.avaldus[0].attributes.avalduse_nimi}</div>
          </div>
        </div>
      </div>
    </div>
  );
}
