import React from 'react';

export default function VehiclesSection({ vehiclesContent }) {
  const bulletpoints = vehiclesContent.bulletpoints;

  return (
    <div className="page-container">
      <div className="content-container">
        <div className="svg-container"></div>
        <div className="content-wrapper">
          <div className="content-title"> {vehiclesContent.title}</div>
          <div className="rectangle" />
          <div className="content-table">
            {bulletpoints.map((item, key) => (
              <div key={key}>
                <div className="icon"></div>
                <div className="content"> {item.attributes.bulletpoint}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
