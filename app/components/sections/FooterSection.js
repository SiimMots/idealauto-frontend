import React from 'react';


export default function footerSection() {

    return (
        <div className="footer">
        <div className="footer_container">
          <div className="footer_logo"></div>
          <div className="footer_contact">
            <ul className="contact-flex">
              <li className="time-flex">
                <div className="icon_container">
                  <div className="time_icon"> </div>
                </div>
                <p>E–R 8–17 / AUTOMÜÜK 9–18, L 10–14 / PESUTÄNAV 24–7</p>
              </li>
              <li className="location-flex">
                <div className="icon_container">
                  <div className="location_icon"> </div>
                </div>
                <p>PETERBURI TEE 47/1, TALLINN 11415</p>
              </li>
              <li className="phone-flex">
                <div className="icon_container">
                  <div className="phone_icon"> </div>
                </div>
                <p>+372 663 0873</p>
              </li>
              <li className="social-flex">
                <div className="icon_container">
                  <div className="social_icon"> </div>
                </div>
                <p>FACEBOOK</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    

    );
}