import React from 'react';
import { AppDataContext } from '@optimistdigital/create-frontend/universal-react';
import _ from 'lodash';
import { NavLink } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default function HeaderSection() {
  const {
    pageData: { locales, content },
  } = React.useContext(AppDataContext);

  const activeLocale = _.pick(locales.locales, locales.active);
  const availableLocales = _.omit(locales.locales, locales.active);

  return (
    <div>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:600&display=swap" rel="stylesheet"></link>
      <div className="header">
        <div className="innerheader">
          <div className="logo_container">
            <a href="/">
              {' '}
              <img src={require('app/Disain/logo.png')} style={{ width: '207px', height: '38px' }} />{' '}
            </a>
          </div>

          <nav className="header_nav">
            <ul className="header_nav_items">
              <li className="nav_item_dropdown">
                <Link className="menu_item" to="/">
                  TEENUSED
                </Link>

                <div className="dropdown">
                  <Link to="/autod">Kasutatud autode müük</Link>
                  <a href="#">Automaat ja käsipesu</a>
                  <a href="#">Rehvitöökoda</a>
                  <a href="#">Kere- ja värvitööd</a>
                  <a href="#">Kahjukäsitlus</a>
                  <a href="#">Pane auto müüki</a>
                </div>
              </li>
              <li className="nav_item">
                <a className="menu_item" href="#">
                  {' '}
                  FIRMAST
                </a>
              </li>

              <li className="nav_item">
                <a className="menu_item" href="#">
                  {' '}
                  NIPID
                </a>
              </li>

              <li className="nav_item">
                <Link className="menu_item" to="/kontakt">
                  {' '}
                  KONTAKT
                </Link>
              </li>
            </ul>
          </nav>

          <div className="burger">
            <div className="line1"></div>
            <div className="line2"></div>
            <div className="line3"></div>
          </div>
        </div>
      </div>

      <div className="random_shape">
        <div className="triangle"></div>
        <div className="locale-container">
          <LangSwitcherItem
            locale={Object.values(activeLocale)}
            localeKey={Object.keys(activeLocale)}
            path={content.localePaths[Object.keys(activeLocale)]}
            style={'active-lang'}
          />

          <div className="unactive-locale">
            {availableLocales &&
              Object.entries(availableLocales).map(([localeKey, locale]) => {
                return (
                  <LangSwitcherItem
                    locale={locale}
                    localeKey={localeKey}
                    key={localeKey}
                    path={content.localePaths[localeKey]}
                  />
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
}

function LangSwitcherItem({ locale, localeKey, style, path }) {
  return (
    <div className={`lang-item ${style ?? ''}`} id={localeKey}>
      <NavLink to={path || ' '}>{locale}</NavLink>
    </div>
  );
}
