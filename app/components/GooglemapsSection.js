import React from 'react';

export default function SectionContent() {
  return (
    <div className="googlemaps">
      <div className="map">
        <div className="map_canvas">
          <iframe
            width="100%"
            height="600"
            id="map_canvas"
            src="https://maps.google.com/maps?q=idealauto&t=&z=13&ie=UTF8&iwloc=&output=embed"
            frameBorder="0"
            scrolling="no"
            marginHeight="0"
            marginWidth="0"
          ></iframe>
          <a href="https://www.embedgooglemap.net/blog/booking.com-coupon/">embedgooglemap.net</a>
        </div>
      </div>
      <div className="streetview"></div>
    </div>
  );
}
