// Import SRLWrapper
import { SRLWrapper } from 'simple-react-lightbox';
import React from 'react';
import { useHistory } from 'react-router-dom';

export const BackButton = () => {
  let history = useHistory();

  return (
    <>
      {' '}
      <button style={{ float: 'left', marginLeft: '140px' }} onClick={() => history.goBack()}>
        Tagasi
      </button>{' '}
    </>
  );
};

export default function ListingView(data) {
  const listing = data.data;
  const gallery = listing.gallery;

  return (
    <div className="listing-container">
      <BackButton />
      <div className="listing-wrapper">
        {' '}
        <div className="left-section">
          <div className="listing-header"> {listing.model}</div>

          <div className="specs-container">
            <div className="attributes-wrapper">
              <div className="attribute">Läbisõit:</div>

              <div className="attribute">Keretüüp: </div>
              <div className="attribute">Vedav sild:</div>
              <div className="attribute">Aasta:</div>
              <div className="attribute">Kütusetüüp:</div>
              <div className="attribute">Käigukast:</div>
              <div className="attribute">Hind:</div>
            </div>

            <div className="spec-wrapper">
              <div className="specification">{listing.mileage + ' KM'}</div>
              <div className="specification">{listing.bodyType}</div>
              <div className="specification">{listing.drive}</div>
              <div className="specification">{listing.year}</div>
              <div className="specification">{JSON.parse(listing.fuelType)}</div>
              <div className="specification">{JSON.parse(listing.gearbox)}</div>
              <div className="price">{listing.price + ' eur'}</div>
            </div>
          </div>
        </div>
        <div className="right-section">
          <SRLWrapper className="lightbox">
            {' '}
            <img src={listing.thumbnail} alt="Thumbnail" style={{ width: '90%', height: '340px' }} />
            {gallery.map((item) => (
              <img src={item.url} style={{ width: '116px', height: '90px', marginRight: '10.1px' }} />
            ))}
          </SRLWrapper>
        </div>
      </div>
    </div>
  );
}
