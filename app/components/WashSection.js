import React from 'react';
// import { AppDataContext } from "@optimistdigital/create-frontend/universal-react";

export default function SectionContent({ washContent }) {
  // const wash = content.data.content.find((item) => item.layout === "pesula");

  const washHexagon = washContent.hexagon;

  return (
    <div className="pesutänav">
      <div className="bitmap">
        <div className="pesutänav_hexagon_container">
          <div className="hexagon_top">
            <div className="top_left">
              <p> {washHexagon[0].attributes.hexagoni_sisu} </p>
            </div>
            <div className="top_right">
              <p> {washHexagon[1].attributes.hexagoni_sisu}</p>
            </div>
          </div>
          <div className="hexagon_bottom">
            <div className="bottom">
              <p>{washHexagon[2].attributes.hexagoni_sisu}</p>
            </div>
          </div>
        </div>

        <div className="pesutänav_table">
          <div className="pesutänav_table_header">
            <p>{washContent.pealkiri}</p>
          </div>

          <div className="pesutänav_table_rectangle"> </div>
          <div className="pesutänav_table_text">
            <p>{washContent.kirjeldus}</p>
          </div>

          <div className="pesutänav_mobile_container">
            <div className="pesutänav_mobile">
              <div className="checkmark"></div>
              {washHexagon[0].attributes.hexagoni_sisu}
            </div>

            <div className="pesutänav_mobile">
              <div className="checkmark"></div>
              {washHexagon[1].attributes.hexagoni_sisu}
            </div>

            <div className="pesutänav_mobile">
              <div className="checkmark"></div>
              {washHexagon[2].attributes.hexagoni_sisu}
            </div>
          </div>

          <div className="pesutänav_table_button">
            <ul className="pesutänav_button">
              <li className="button4">
                {' '}
                <p> {washContent.nupud[0].attributes.nupu_pealkiri} </p>{' '}
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
