import React from 'react';

export default function SectionContent({ heroContent }) {
  // const heroContent = content.data.content.find((item) => item.layout === 'esileht');

  const servicesContainer = heroContent.teenused;

  return (
    <div className="background">
      <div className="background_bitmap">
        <div className="burger-wrapper">
          <div className="burger-menu-wrapper">
            <div className="burger-menu">
              <div className="burger-close">
                <div className="cross1"></div>
                <div className="cross2"></div>
              </div>
              <nav className="bm-items">
                <ul className="bm-nav-items">
                  <a className="bm-dropdown-button" href="#">
                    TEENUSED
                  </a>

                  <div className="bm-dropdown">
                    <a href="#">KASUTATUD AUTODE MÜÜK</a>
                    <a href="#">AUTOMAAT JA KÄSIPESU</a>
                    <a href="#">REHVITÖÖKODA</a>
                    <a href="#">KERE- JA VÄRVITÖÖD</a>
                    <a href="#">KAHJUKÄSITLUS</a>
                    <a href="#">PANE AUTO MÜÜKI</a>
                  </div>

                  <a className="bm-item" href="#">
                    {' '}
                    FIRMAST
                  </a>

                  <a className="bm-item" href="#">
                    {' '}
                    NIPID
                  </a>

                  <a className="bm-item" href="#">
                    {' '}
                    KONTAKT
                  </a>
                </ul>
              </nav>
            </div>
          </div>
        </div>

        <div className="background_honeycomb"></div>
        <div className="background_container">
          <div className="text_container">
            <div className="text">
              <p> {heroContent.title}</p>
            </div>
            <div className="avarii_table_rectangle"></div>
            <div className="text_name">
              <p> {heroContent.tutvustus}</p>
            </div>
          </div>
        </div>

        <div className="background_container">
          <div className="button_container">
            <div className="button_flex">
              {servicesContainer.map((
                item,
                key // loop teenused
              ) => (
                <div key={key}>
                  <li className="flex-item">
                    {' '}
                    <p>{item.attributes.teenus}</p>
                    <div className="chevron_container"></div>
                  </li>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="contact_container">
          <ul className="contact-flex">
            <li className="time-flex">
              <div className="icon_container">
                <div className="time_icon"> </div>
              </div>

              <p> {heroContent.lahtiolekuaeg[0].attributes.lahtiolekuaeg} </p>
            </li>
            <li className="location-flex">
              <div className="icon_container">
                <div className="location_icon"> </div>
              </div>
              <p> {heroContent.aadress[0].attributes.aadress} </p>
            </li>
            <li className="phone-flex">
              <div className="icon_container">
                <div className="phone_icon"> </div>
              </div>
              <p>{heroContent.telefoninumber[0].attributes.telefoninumber}</p>
            </li>
            <li className="social-flex">
              <div className="icon_container">
                <div className="social_icon"> </div>
              </div>
              <p> {heroContent.sotsiaalmeedia[0].attributes.sotsiaalmeedia} </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
