import 'app/scss/entry.scss';
import { Helmet } from 'react-helmet-async';
import { Switch, Route } from 'react-router-dom';
import ErrorBoundary from 'app/components/ErrorBoundary';
import ErrorPage from 'app/pages/ErrorPage';
import React from 'react';
import Router, { getRouteData } from '@optimistdigital/create-frontend/universal-react/Router';
import Routes from 'app/routes';
import fetchContent from 'app/api/fetchContent';
import './fonts/Montserrat/Montserrat-Light.ttf';
import './fonts/Montserrat/Montserrat-Bold.ttf';

export default function App() {
  return (
    <React.Fragment>
      <Helmet>
        <title>New app</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Helmet>

      <ErrorBoundary renderError={() => <ErrorPage status={500} />}>
        <Router>
          <Routes />
        </Router>
      </ErrorBoundary>
    </React.Fragment>
  );
}

/**
 * This function gets called once in the server, and in the client whenever the page changes.
 * The result ends up in the AppDataContext.
 */
App.getPageData = async (location, props) => {
  try {
    const content = await fetchContent(location.pathname, location.search);

    return (prevState) => ({
      ...prevState,
      content: content?.content,
      config: prevState.config || props.config,

      locales: content?.locales ?? null,
      activeLocale: content?.locales?.active ?? null,
      redirect: content?.redirect ?? null,
      statusCode: 200,
      pathname: location.pathname,
    });
  } catch (err) {
    console.error('caught error while fetching pageData on server', err);
    return (prevState) => ({
      ...prevState,
      config: prevState.config || props.config,
      statusCode: (err.response ? err.response.status : err.status) ?? 500,
    });
  }
};
