import React from 'react';

import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import { Helmet } from 'react-helmet-async';
import HeaderSection from '../components/sections/HeaderSection';
import FooterSection from '../components/sections/FooterSection';
import GoogleMapsSection from '../components/GooglemapsSection';

import ListingView from '../components/ListingView';
import SimpleReactLightbox from 'simple-react-lightbox';

export default function ListingPage() {
  const {
    pageData: { content },
  } = React.useContext(AppDataContext);

  const data = content?.listing ?? null;

  return (
    <SimpleReactLightbox>
      <React.Fragment>
        <Helmet>
          <title>Listing</title>
        </Helmet>

        <HeaderSection />
        <ListingView data={data} />
        <GoogleMapsSection />
        <FooterSection />
      </React.Fragment>
    </SimpleReactLightbox>
  );
}

/*
function VehiclePageContent({ data }) {
  const contentTypes = (attributes, layout) => {
    switch (layout) {
      case 'description':
        return <VehiclesSection vehiclesContent={attributes} />;
      case 'listings':
        return <UsedCarListings />;
      default:
        return null;
    }
  };
  

  return Object.values(data).map((object) => {
    const component = contentTypes(object.attributes, object.layout) ?? null;
    return component;
  });
}
*/
