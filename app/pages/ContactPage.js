import React from 'react';
import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import { Helmet } from 'react-helmet-async';

import HeaderSection from '../components/sections/HeaderSection';
import FooterSection from '../components/sections/FooterSection';

import ContactInformation from '../components/ContactSection';
import ContactForm from '../components/ContactForm';
import GooglemapsSection from '../components/GooglemapsSection';

export default function ContactPage() {
  const {
    pageData: { content },
  } = React.useContext(AppDataContext);

  const data = content?.data?.content ?? null;

  return (
    <React.Fragment>
      <Helmet>
        <title>Contact - {content?.name}</title>
      </Helmet>
      <HeaderSection />
      <ContactPageContent data={data} />

      {/* }
      <ContactInformation />
      <ContactForm />
      <GooglemapsSection />
    */}
      <FooterSection />
    </React.Fragment>
  );
}

function ContactPageContent({ data }) {
  const contentTypes = (attributes, layout) => {
    switch (layout) {
      case 'kontaktandmed':
        return <ContactInformation contactContent={attributes} />;
      case 'email':
        return <ContactForm formContent={attributes} />;
      case 'kontor':
        return <GooglemapsSection />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object) => {
    const component = contentTypes(object.attributes, object.layout) ?? null;
    return component;
  });
}
