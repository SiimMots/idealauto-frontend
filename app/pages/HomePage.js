import React from 'react';
// import fetchContent from 'app/api/fetchContent';
// import bigCar from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF0279.jpg';
// import smallCar1 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF7228-1.jpg';
// import smallCar2 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF8954.jpg';
// import smallCar3 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF9482-1.jpg';
// import smallCar4 from 'app/Disain/ideal pildid weebi/uued pildid 5 märts 2019/avalehe galerii/_DSF9625_resize.JPG';

import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import { Helmet } from 'react-helmet-async';

import HeaderSection from '../components/sections/HeaderSection';
import HeroSection from '../components/HeroSection';
import TradeSection from '../components/TradeSection';
import CrashSection from '../components/CrashSection';
import WashSection from '../components/WashSection';
import GallerySection from '../components/GallerySection';
import AboutSection from '../components/AboutSection';
import GooglemapsSection from '../components/GooglemapsSection';
import FooterSection from '../components/sections/FooterSection';

export default function HomePage() {
  const {
    pageData: { content },
  } = React.useContext(AppDataContext);

  const data = content?.data?.content ?? null;

  return (
    <React.Fragment>
      <Helmet>
        <title>Ideal Auto - {content?.name}</title>
      </Helmet>

      <HeaderSection />
      {/* }
      <HeroSection />
      <TradeSection />
      <CrashSection />
      <WashSection />
      <GallerySection />
      <AboutSection />
      <GooglemapsSection />
      <FooterSection />
      */}
      <HomePageContent data={data} />
      <FooterSection />
    </React.Fragment>
  );
}

function HomePageContent({ data }) {
  const contentTypes = (attributes, layout) => {
    switch (layout) {
      case 'esileht':
        return <HeroSection heroContent={attributes} />;
      case 'autovahetus':
        return <TradeSection tradeContent={attributes} />;
      case 'avarii':
        return <CrashSection crashContent={attributes} />;
      case 'pesula':
        return <WashSection washContent={attributes} />;
      case 'galerii':
        return <GallerySection />;
      case 'firmast':
        return <AboutSection aboutContent={attributes} />;
      case 'kontor':
        return <GooglemapsSection />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object) => {
    const component = contentTypes(object.attributes, object.layout) ?? null;
    return component;
  });
}
/*
    <div>
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"></link>

      <div className="header">
        <div className="innerheader">
          <div className="logo_container">
            <h1>
              {' '}
              <img src={require('app/Disain/logo.png')} style={{ width: '207px', height: '38px' }} />{' '}
            </h1>
          </div>

          <nav className="header_nav">
            <ul className="header_nav_items">
              <li className="nav_item_dropdown">
                <a className="menu_item" href="#">
                  TEENUSED
                </a>

                <div className="dropdown">
                  <a href="#">Kasutatud autode müük</a>
                  <a href="#">Automaat ja käsipesu</a>
                  <a href="#">Rehvitöökoda</a>
                  <a href="#">Kere- ja värvitööd</a>
                  <a href="#">Kahjukäsitlus</a>
                  <a href="#">Pane auto müüki</a>
                </div>
              </li>
              <li className="nav_item">
                <a className="menu_item" href="#">
                  {' '}
                  FIRMAST
                </a>
              </li>

              <li className="nav_item">
                <a className="menu_item" href="#">
                  {' '}
                  NIPID
                </a>
              </li>

              <li className="nav_item">
                <a className="menu_item" href="#">
                  {' '}
                  KONTAKT
                </a>
              </li>
            </ul>
          </nav>

          <div className="burger">
            <div className="line1"></div>
            <div className="line2"></div>
            <div className="line3"></div>
          </div>
        </div>
      </div>

      <div className="random_shape">
        <div className="triangle"></div>
      </div>

      <div className="background">
        <div className="background_bitmap">
          <div className="burger-wrapper">
            <div className="burger-menu-wrapper">
              <div className="burger-menu">
                <div className="burger-close">
                  <div className="cross1"></div>
                  <div className="cross2"></div>
                </div>
                <nav className="bm-items">
                  <ul className="bm-nav-items">
                    <a className="bm-dropdown-button" href="#">
                      TEENUSED
                    </a>

                    <div className="bm-dropdown">
                      <a href="#">KASUTATUD AUTODE MÜÜK</a>
                      <a href="#">AUTOMAAT JA KÄSIPESU</a>
                      <a href="#">REHVITÖÖKODA</a>
                      <a href="#">KERE- JA VÄRVITÖÖD</a>
                      <a href="#">KAHJUKÄSITLUS</a>
                      <a href="#">PANE AUTO MÜÜKI</a>
                    </div>

                    <a className="bm-item" href="#">
                      {' '}
                      FIRMAST
                    </a>

                    <a className="bm-item" href="#">
                      {' '}
                      NIPID
                    </a>

                    <a className="bm-item" href="#">
                      {' '}
                      KONTAKT
                    </a>
                  </ul>
                </nav>
              </div>
            </div>
          </div>

          <div className="background_honeycomb"></div>
          <div className="background_container">
            <div className="text_container">
              <div className="text">
                <p>
                  {' '}
                  <span>LAHE</span>NDUSED LIIKVEL PÜSIMISEKS
                </p>
              </div>
              <div className="avarii_table_rectangle"></div>
              <div className="text_name">
                <p> AUTODE MÜÜGI- JA TEENINDUSKESKUS</p>
              </div>
            </div>
          </div>

          <div className="background_container">
            <div className="button_container">
              <ul className="button_flex">
                <li className="flex-item">
                  <p>AUTODE MÜÜK</p>
                  <div className="chevron_container"></div>
                </li>
                <li className="flex-item">
                  <p>KERE- JA VÄRVITÖÖD</p>
                  <div className="chevron_container"></div>
                </li>
                <li className="flex-item">
                  <p>KAHJUKÄSITLUS</p>
                  <div className="chevron_container"></div>
                </li>
                <li className="flex-item">
                  <p>REHVIVAHETUS</p>
                  <div className="chevron_container"></div>
                </li>
                <li className="flex-item">
                  <p>PESUTÄNAV JA KÄSIPESU</p>
                  <div className="chevron_container"></div>
                </li>
                <li className="flex-item">
                  <p>KONTAKT</p>
                  <div className="chevron_container"></div>
                </li>
              </ul>
            </div>
          </div>

          <div className="contact_container">
            <ul className="contact-flex">
              <li className="time-flex">
                <div className="icon_container">
                  <div className="time_icon"> </div>
                </div>
                <p>E–R 8–17 / AUTOMÜÜK 9–18, L 10–14 / PESUTÄNAV 24–7</p>
              </li>
              <li className="location-flex">
                <div className="icon_container">
                  <div className="location_icon"> </div>
                </div>
                <p>PETERBURI TEE 47/1, TALLINN 11415</p>
              </li>
              <li className="phone-flex">
                <div className="icon_container">
                  <div className="phone_icon"> </div>
                </div>
                <p>+372 663 0873</p>
              </li>
              <li className="social-flex">
                <div className="icon_container">
                  <div className="social_icon"> </div>
                </div>
                <p>FACEBOOK</p>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="vahetus">
        <div className="vahetus-honeycomb">
          <div className="honeycomb2"> </div>
        </div>

        <div className="content_right">
          <div className="vahetus_container">
            <div className="vahetus-heading">
              <p>
                {' '}
                <span>AUTOVAHETUS</span> TEHTUD VALUTUKS{' '}
              </p>
            </div>

            <div className="vahetus_rectangle"></div>
          </div>

          <div className="vahetus_container">
            <div className="vahetus-text">
              <p>
                Inimeste jaoks, kes sellega pidevalt kokku ei puutu, võib auto vahetamine olla tükk tegemist. Meie
                arvates peaks see aga olema tore sündmus, mis iga mõne aasta tagant ellu veidi elevust toob.
              </p>
            </div>
          </div>

          <div className="vahetus_container">
            <div className="vahetus-info">
              <div className="info-flex">
                <div className="list_item">
                  <div className="diamond_container">
                    <div className="diamond"></div>
                  </div>
                  Viime su olemasoleva masina parimasse müügivormi ja paneme müüki
                </div>

                <div className="list_item">
                  <div className="diamond_container">
                    <div className="diamond"></div>
                  </div>
                  Tutvustame sulle soojas puhtas salongis esinduslikku valikut garantiilisi vähekasutatud autosid
                </div>
              </div>
            </div>
          </div>

          <div className="vahetus_container">
            <div className="vahetus-buttons">
              <ul className="buttons">
                <li className="button1">
                  <p>PANE AUTO MÜÜKI</p>
                </li>
                <li className="button2">
                  <p>VAATA AUTOSID</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="avarii">
        <div className="avarii_image_container">
          <div className="avarii_image_honeycomb"> </div>
        </div>

        <div className="avarii_container">
          <div className="avarii_table">
            <div className="avarii_table_header">
              <p>
                KUI SATUD AVARIISSE, <span>VÕTA ÜHENDUST!</span>
              </p>
            </div>

            <div className="avarii_table_rectangle"></div>

            <div className="avarii_table_list">
              <div className="avarii_list_item">
                <div className="checkmark"></div>
                Aitame kahjustada saanud auto töökotta
              </div>

              <div className="avarii_list_item">
                <div className="checkmark"></div>
                Hindame kahju
              </div>

              <div className="avarii_list_item">
                <div className="checkmark"></div>
                Suhtleme kindlustusega
              </div>

              <div className="avarii_list_item">
                <div className="checkmark"></div>
                Varustame asendussõidukiga
              </div>

              <div className="avarii_list_item">
                <div className="checkmark"></div>
                Teeme masina korda
              </div>
            </div>
            <div className="avarii_table_buttons">
              <ul className="avarii_buttons">
                <li className="button1">
                  {' '}
                  <p>RÄÄGI TEENINDAJAGA</p>
                </li>
                <li className="button3">
                  {' '}
                  <p>HELISTA</p>
                </li>
              </ul>
            </div>

            <div className="kahjuavaldus_container">
              <div className="chevron"></div>
              <div className="kahjuavaldus">TÄIDA KAHJUAVALDUS</div>
            </div>
          </div>
        </div>
      </div>

      <div className="pesutänav">
        <div className="bitmap">
          <div className="pesutänav_hexagon_container">
            <div className="hexagon_top">
              <div className="top_left">
                <p>LIGIPÄÄS 24/7</p>
              </div>
              <div className="top_right">
                <p>KÄRCHERI KVALITEET</p>
              </div>
            </div>
            <div className="hexagon_bottom">
              <div className="bottom">
                <p>NUMBRI- TUVASTUS</p>
              </div>
            </div>
          </div>

          <div className="pesutänav_table">
            <div className="pesutänav_table_header">
              <p>
                {' '}
                <span1>LAHE</span1>
                <span2>NDUSED,</span2>
                MITTE LAHENDUSED
              </p>
            </div>

            <div className="pesutänav_table_rectangle"> </div>
            <div className="pesutänav_table_text">
              <p>
                Tehnikafännid nagu me siin oleme, tahame sulle pakkuda selle viimast sõna. Näiteks numbrituvastusega
                täisautomaatset pesutänavat, mis saadab sulle arve alles kuu lõpus.
              </p>
            </div>

            <div className="pesutänav_mobile_container">
              <div className="pesutänav_mobile">
                <div className="checkmark"></div>
                Ligipääs 24/7
              </div>

              <div className="pesutänav_mobile">
                <div className="checkmark"></div>
                Kärcheri kvaliteet
              </div>

              <div className="pesutänav_mobile">
                <div className="checkmark"></div>
                Numbrituvastus
              </div>
            </div>

            <div className="pesutänav_table_button">
              <ul className="pesutänav_button">
                <li className="button4">
                  {' '}
                  <p>SÕLMI LEPING</p>{' '}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="cars_container">
        <div className="single-container">
          <div className="big-car">
            <div style={{ backgroundImage: `url(${bigCar})` }}></div>
          </div>
        </div>

        <div className="multi-container">
          <div className="small-car">
            <div style={{ backgroundImage: `url(${smallCar1})` }}></div>
          </div>
          <div className="small-car">
            <div style={{ backgroundImage: `url(${smallCar2})` }}></div>
          </div>
          <div className="small-car">
            <div style={{ backgroundImage: `url(${smallCar3})` }}></div>
          </div>
          <div className="small-car">
            <div style={{ backgroundImage: `url(${smallCar4})` }}></div>
          </div>
        </div>
      </div>

      <div className="firmast">
        <div className="firmast_table">
          <div className="firmast_table_header">
            <p>Ega me endale pulli pärast sellist nime pannud</p>
            <div className="long_rectangle"></div>
          </div>
          <div className="squares">
            <div className="square"></div>
            <div className="square"></div>
            <div className="square"></div>
          </div>
          <div className="firmast_text">
            <p>
              Olles eluaeg autodega tegelenud, oskame hinnata seda uhkustunnet ja kindlust, mida pakub puhas ning korras
              masin. Juba üle aastakümne Baltikumi suurima rendiautode pargiga tegeledes oleme omandanud valdkonna
              parima võimaliku kogemuse, mille toome nüüd Ideal Auto müügi- ja teeninduskeskuse kaudu kõigi soovijateni.
            </p>
          </div>

          <div className="firmast_button">
            <p>FIRMAST</p>
          </div>
        </div>
      </div>

      <div className="googlemaps">
        <div className="map">
          <div className="map_canvas">
            <iframe
              width="100%"
              height="600"
              id="map_canvas"
              src="https://maps.google.com/maps?q=idealauto&t=&z=13&ie=UTF8&iwloc=&output=embed"
              frameBorder="0"
              scrolling="no"
              marginHeight="0"
              marginWidth="0"
            ></iframe>
            <a href="https://www.embedgooglemap.net/blog/booking.com-coupon/">embedgooglemap.net</a>
          </div>
        </div>
        <div className="streetview"></div>
      </div>

      <div className="footer">
        <div className="footer_container">
          <div className="footer_logo"></div>
          <div className="footer_contact">
            <ul className="contact-flex">
              <li className="time-flex">
                <div className="icon_container">
                  <div className="time_icon"> </div>
                </div>
                <p>E–R 8–17 / AUTOMÜÜK 9–18, L 10–14 / PESUTÄNAV 24–7</p>
              </li>
              <li className="location-flex">
                <div className="icon_container">
                  <div className="location_icon"> </div>
                </div>
                <p>PETERBURI TEE 47/1, TALLINN 11415</p>
              </li>
              <li className="phone-flex">
                <div className="icon_container">
                  <div className="phone_icon"> </div>
                </div>
                <p>+372 663 0873</p>
              </li>
              <li className="social-flex">
                <div className="icon_container">
                  <div className="social_icon"> </div>
                </div>
                <p>FACEBOOK</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
  */

/*
HomePage.getPageData = async (location, props) => {
    try {
      const content = await fetchContent(location.pathname, location.search);
      return (prevState) => ({
        ...prevState,
        content: content?.content,

      });
    } catch (err) {
      console.error('Caught error while fetching data on server:', err);
      return (prevState) => ({
        ...prevState,
        config: prevState.config || props.config,
        statusCode: (err.response ? err.response.status : err.status) ?? 500,
      });
    }
  };
  

HomePage.getPageData = async (/* location, params, props ) => {
  try {
    const content = await fetchContent();

    return (prevState) => ({
      ...prevState,
      content: content?.content,
    });
  } catch (err) {
    console.error('caught error while fetching pageData on server', err);
  }

  return (prevState) => ({
    ...prevState,
    // Add data here that will be added whenever the user navigates to this page
  });
  */
