import React from 'react';
import { AppDataContext } from '@optimistdigital/create-frontend/universal-react';
import HomePage from './HomePage';
import ContactPage from './ContactPage';
import ErrorPage from './ErrorPage';
import ListingPage from './ListingPage';

import { Redirect } from 'react-router-dom';
import VehiclePage from './VehiclePage';

export default function CmsComponent() {
  React.useEffect(() => {
    const linkTags = document.querySelectorAll('a');
    linkTags.forEach((tag) => {
      tag.setAttribute('target', '_blank');
    });
  });

  const templates = {
    homepage: HomePage,
    contactpage: ContactPage,
    vehiclepage: VehiclePage,
    listing: ListingPage,
  };

  const { content, redirect } = React.useContext(AppDataContext).pageData;

  if (redirect && redirect.to_url && redirect.from_url)
    return <Redirect exact to={redirect.to_url} from={redirect.to_url} />;

  const getComponent = (page) => templates[page] ?? null;
  const Component = getComponent(content?.template);
  // console.log(Component);
  return Component ? <Component /> : <ErrorPage />;
}
