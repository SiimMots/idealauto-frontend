import React from 'react';

export default function ErrorPage({ status }) {
  const messages = {
    '404': 'Page not found',
  };

  return (
    <div className="error-container">
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"></link>
      <div className="error-wrapper">
        <h1 style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
          {messages[status] || 'Error 404: Page not found'}
        </h1>
      </div>
    </div>
  );
}
