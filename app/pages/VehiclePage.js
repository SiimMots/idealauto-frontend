import React from 'react';

import AppDataContext from '@optimistdigital/create-frontend/universal-react/AppDataContext';
import { Helmet } from 'react-helmet-async';
import HeaderSection from '../components/sections/HeaderSection';
import FooterSection from '../components/sections/FooterSection';
import VehiclesSection from '../components/VehiclesSection';
import UsedCarListings from '../components/UsedCarSection';
import ListingView from '../components/ListingView';
import SimpleReactLightbox from 'simple-react-lightbox';

export default function VehiclePage() {
  const {
    pageData: { content },
  } = React.useContext(AppDataContext);

  const data = content?.data?.content ?? null;

  return (
    <SimpleReactLightbox>
      <React.Fragment>
        <Helmet>
          <title>{content?.name}</title>
        </Helmet>

        <HeaderSection />
        <VehiclePageContent data={data} />

        <FooterSection />
      </React.Fragment>
    </SimpleReactLightbox>
  );
}

function VehiclePageContent({ data }) {
  const contentTypes = (attributes, layout) => {
    switch (layout) {
      case 'description':
        return <VehiclesSection vehiclesContent={attributes} />;
      case 'listings':
        return <UsedCarListings />;
      default:
        return null;
    }
  };

  return Object.values(data).map((object) => {
    const component = contentTypes(object.attributes, object.layout) ?? null;
    return component;
  });
}
