import React from 'react';
import { Switch, Route } from 'react-router-dom';
import CmsComponent from './pages/CmsComponent';
import ErrorPage from './pages/ErrorPage';

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/error" component={ErrorPage} />
      <Route component={CmsComponent} />
    </Switch>
  );
}
