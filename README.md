# idealauto-react

This is a server-rendered React app that uses [create-frontend](https://github.com/optimistdigital/create-frontend/blob/master/docs/universal-react.md).

## Development

- `npm ci` - install dependencies
- `npm run dev` - start local development server

## Production

- `npm ci` - install dependencies
- `npm run build` - build the app
- `npm run serve` - start the node server
