/**
 * This is the app config file. It's defined in the server, so you can access process.env.
 * In React, AppDataContext can be used to access this config in both the client and server.
 */
module.exports = () => ({
  // CMS_URL: 'http://127.0.0.1:8000',
});
